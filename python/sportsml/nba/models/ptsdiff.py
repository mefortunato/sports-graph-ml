import dgl
import torch
from .base import BaseModel
from sklearn.preprocessing import StandardScaler


class PtsDiffModel(BaseModel):
    def __init__(
        self,
        feat_scaler: StandardScaler = None,
        target_scaler: StandardScaler = None,
    ):
        super().__init__()
        self.feat_scaler = feat_scaler
        self.target_scaler = target_scaler
        self.save_hyperparameters("feat_scaler", "target_scaler")

    def forward(self, graph, label_graph):
        h = (graph.ndata["stats"] - graph.ndata["opp_stats"])[:, 0]
        src, dst = label_graph.edges()
        h_src = h[src]
        h_dst = h[dst]
        h = h_dst - h_src
        return h.reshape(-1, 1)
