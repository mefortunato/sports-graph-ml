import dgl
import torch
from .base import BaseModel
from sklearn.preprocessing import StandardScaler


class PtsNNModel(BaseModel):
    def __init__(
        self,
        in_feats=4,
        hidden_feats=32,
        out_dim=1,
        feat_scaler: StandardScaler = None,
        target_scaler: StandardScaler = None,
    ):
        super().__init__()
        self.nn = torch.nn.Sequential(
            torch.nn.Linear(in_feats, hidden_feats),
            torch.nn.ReLU(),
            torch.nn.Dropout(0.1),
            torch.nn.Linear(hidden_feats, hidden_feats),
            torch.nn.ReLU(),
            torch.nn.Dropout(0.1),
            torch.nn.Linear(hidden_feats, out_dim),
        )
        self.feat_scaler = feat_scaler
        self.target_scaler = target_scaler
        self.save_hyperparameters("feat_scaler", "target_scaler")

    def forward(self, graph, label_graph):
        h = torch.hstack([graph.ndata["stats"][:, 0], graph.ndata["opp_stats"][:, 0]])
        src, dst = label_graph.edges()
        h_src = h[src]
        h_dst = h[dst]
        h = torch.hstack([h_src, h_dst])
        return self.nn(h)
