import dgl
import torch
from .base import BaseModel
from sklearn.preprocessing import StandardScaler


class CatNNModel(BaseModel):
    def __init__(
        self,
        in_feats=58,
        hidden_feats=32,
        out_dim=1,
        feat_scaler: StandardScaler = None,
        target_scaler: StandardScaler = None,
    ):
        super().__init__()
        self.nn = torch.nn.Sequential(
            torch.nn.Linear(in_feats, hidden_feats),
            torch.nn.ReLU(),
            torch.nn.Dropout(0.1),
            torch.nn.Linear(hidden_feats, hidden_feats),
            torch.nn.ReLU(),
            torch.nn.Dropout(0.1),
            torch.nn.Linear(hidden_feats, out_dim),
        )
        self.feat_scaler = feat_scaler
        self.target_scaler = target_scaler
        self.save_hyperparameters(
            "in_feats", "hidden_feats", "out_dim", "feat_scaler", "target_scaler"
        )

    def forward(self, graph, label_graph):
        h = torch.hstack([graph.ndata["stats"], graph.ndata["opp_stats"]])
        src, dst = label_graph.edges()
        home = label_graph.edata["home"]
        rest = label_graph.edata["rest"]
        h_src = h[src]
        h_dst = h[dst]
        h = torch.cat([h_src, h_dst, home, rest], dim=1)
        h = self.nn(h)
        return h
