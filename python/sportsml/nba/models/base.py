import dgl
import pytorch_lightning as pl
import torch
import torchmetrics
from dgllife.model import HadamardLinkPredictor, WLN
from sklearn.preprocessing import StandardScaler


class BaseModel(pl.LightningModule):
    def __init__(
        self,
        loss_fn=torch.nn.functional.mse_loss,
        mse=torchmetrics.MeanSquaredError(),
        r2=torchmetrics.R2Score(),
    ):
        super().__init__()
        self.loss_fn = loss_fn
        self.mse = mse
        self.r2 = r2
        self.save_hyperparameters("loss_fn")

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

    def forward_pass(self, batch):
        graph, label_graph = batch
        graph = graph.to(self.device)
        label_graph = label_graph.to(self.device)
        preds = self(graph, label_graph)
        targets = label_graph.edata["target"]
        loss = torch.nn.functional.mse_loss(preds, targets, reduction="mean")
        return preds, targets, loss

    def training_step(self, batch, batch_idx):
        preds, targets, loss = self.forward_pass(batch)
        return loss

    def validation_step(self, batch, batch_idx):
        batch_size = batch[1].num_edges()
        preds, targets, loss = self.forward_pass(batch)
        self.log("val_loss", loss, batch_size=batch_size)

        if self.target_scaler is not None:
            preds = self.target_scaler.inverse_transform(preds.detach().cpu().numpy())
            preds = torch.from_numpy(preds).to(self.device)
            targets = self.target_scaler.inverse_transform(targets.cpu().numpy())
            targets = torch.from_numpy(targets).to(self.device)

        self.mse(preds, targets)
        self.log("val_mse", self.mse, batch_size=batch_size)

        self.r2(preds, targets)
        self.log("val_r2", self.r2, batch_size=batch_size)

        return loss

    def test_step(self, batch, batch_idx):
        batch_size = batch[1].num_edges()
        preds, targets, loss = self.forward_pass(batch)
        self.log("test_loss", loss, batch_size=batch_size)

        if self.target_scaler is not None:
            preds = self.target_scaler.inverse_transform(preds.detach().cpu().numpy())
            preds = torch.from_numpy(preds).to(self.device)
            targets = self.target_scaler.inverse_transform(targets.cpu().numpy())
            targets = torch.from_numpy(targets).to(self.device)

        self.mse(preds, targets)
        self.log("test_mse", self.mse, batch_size=batch_size)

        self.r2(preds, targets)
        self.log("test_r2", self.r2, batch_size=batch_size)

        return loss

    def predict_step(self, batch, batch_idx):
        graph, label_graph = batch
        graph = graph.to(self.device)
        label_graph = label_graph.to(self.device)
        preds = self(graph, label_graph)
        return preds
