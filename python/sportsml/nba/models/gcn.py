import dgl
import pytorch_lightning as pl
import torch
import torchmetrics
from dgllife.model import HadamardLinkPredictor, WLN
from sklearn.preprocessing import StandardScaler


class GCNModel(pl.LightningModule):
    def __init__(
        self,
        node_in_feats: int,
        edge_in_feats: int,
        node_out_feats: int = 300,
        n_layers: int = 3,
        out_dim: int = 1,
        feat_scaler: StandardScaler = None,
        target_scaler: StandardScaler = None,
    ):
        super().__init__()
        self.save_hyperparameters(
            "node_in_feats",
            "edge_in_feats",
            "node_out_feats",
            "n_layers",
            "out_dim",
            "feat_scaler",
            "target_scaler",
        )
        self.wln = WLN(
            node_in_feats=node_in_feats,
            edge_in_feats=edge_in_feats,
            node_out_feats=node_out_feats,
            n_layers=n_layers,
        )
        self.pred = dgl.nn.pytorch.link.EdgePredictor("cat", node_out_feats, out_dim)
        self.feat_scaler = feat_scaler
        self.target_scaler = target_scaler
        self.mse = torchmetrics.MeanSquaredError()
        self.r2 = torchmetrics.R2Score()

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

    def forward(self, graph, label_graph):
        h = self.wln(graph, graph.ndata["stats"], graph.edata["stats"])
        src, dst = label_graph.edges()
        h_src = h[src]
        h_dst = h[dst]
        return self.pred(h_src, h_dst)

    def forward_pass(self, batch):
        graph, label_graph = batch
        graph = graph.to(self.device)
        label_graph = label_graph.to(self.device)
        preds = self(graph, label_graph)
        targets = label_graph.edata["plus_minus"]
        loss = torch.nn.functional.mse_loss(preds, targets, reduction="mean")
        return preds, targets, loss

    def training_step(self, batch, batch_idx):
        preds, targets, loss = self.forward_pass(batch)
        return loss

    def validation_step(self, batch, batch_idx):
        batch_size = batch[1].num_edges()
        preds, targets, loss = self.forward_pass(batch)
        self.log("val_loss", loss, batch_size=batch_size)

        if self.target_scaler is not None:
            preds = self.target_scaler.inverse_transform(preds.detach().cpu().numpy())
            preds = torch.from_numpy(preds)
            targets = self.target_scaler.inverse_transform(targets.cpu().numpy())
            targets = torch.from_numpy(targets)

        self.mse(preds, targets)
        self.log("val_mse", self.mse, batch_size=batch_size)

        self.r2(preds, targets)
        self.log("val_r2", self.r2, batch_size=batch_size)

        return loss

    def test_step(self, batch, batch_idx):
        batch_size = batch[1].num_edges()
        preds, targets, loss = self.forward_pass(batch)
        self.log("test_loss", loss, batch_size=batch_size)

        if self.target_scaler is not None:
            preds = self.target_scaler.inverse_transform(preds.detach().cpu().numpy())
            preds = torch.from_numpy(preds)
            targets = self.target_scaler.inverse_transform(targets.cpu().numpy())
            targets = torch.from_numpy(targets)

        self.mse(preds, targets)
        self.log("test_mse", self.mse, batch_size=batch_size)

        self.r2(preds, targets)
        self.log("test_r2", self.r2, batch_size=batch_size)

        return loss
