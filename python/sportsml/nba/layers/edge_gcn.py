import dgl
import dgl.function as fn
import torch
import torch.nn.functional as F
from typing import Callable


class EdgeGCN(torch.nn.Module):
    def __init__(
        self,
        e_feats: int,
        h_feats: int,
        activation: Callable = F.relu,
        dropout: int = 0.1,
    ):
        super().__init__()
        self.edge_linear = torch.nn.Linear(e_feats, h_feats)
        self.node_linear = torch.nn.Linear(h_feats, h_feats)
        self.activation = activation
        self.dropout = dropout

    def forward(self, graph, efeats):
        with graph.local_scope():
            edge_h = self.edge_linear(efeats)
            if self.activation is not None:
                edge_h = self.activation(edge_h)
            edge_h = F.dropout(edge_h, p=self.dropout)
            graph.edata["h"] = edge_h
            graph.update_all(
                message_func=fn.copy_e("h", "h_message"),
                reduce_func=fn.mean("h_message", "h"),
            )
            node_h = self.node_linear(graph.ndata["h"])
            if self.activation is not None:
                node_h = self.activation(node_h)
            node_h = F.dropout(node_h, p=self.dropout)
            return node_h, edge_h
