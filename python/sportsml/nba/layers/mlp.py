from typing import List

import torch


class MLP(torch.nn.Module):
    def __init__(
        self,
        input_dim: int,
        hidden_size: List[int],
        output_dim: int,
        dropout: float = 0.2,
        bias: bool = True,
    ):
        super().__init__()
        hidden_size.insert(0, input_dim)
        hidden_size.append(output_dim)
        self.layers = torch.nn.ModuleList(
            [
                torch.nn.Linear(hidden_size[n], hidden_size[n + 1], bias=bias)
                for n in range(len(hidden_size) - 1)
            ]
        )
        self.dropout = torch.nn.Dropout(p=dropout)
        self.activation = torch.nn.ReLU()

    def forward(self, x):
        for n, layer in enumerate(self.layers):
            x = layer(x)
            if n != len(self.layers) - 1:
                x = self.activation(x)
            x = self.dropout(x)
        return x
