from typing import List

import dgl
import numpy as np
import pandas as pd
import torch

from .features import STATS_COLUMNS
from .utils import process_games


def graph_from_csv(filename: str, **kwargs) -> dgl.DGLGraph:
    games = pd.read_csv(filename)
    games = process_games(games)
    return graph_from_games(games, **kwargs)


def graph_from_games(
    games: pd.DataFrame,
    stats_columns: List[str] = STATS_COLUMNS,
    label_columns: List[str] = ["PLUS_MINUS"],
    date_column: str = "GAME_DATE",
    season_column: str = "SEASON_ID",
) -> dgl.DGLGraph:
    g = dgl.graph((games["src"].values, games["target"].values), num_nodes=30)

    g.edata["stats"] = torch.from_numpy(games[stats_columns].values.astype(np.float32))
    g.update_all(
        dgl.function.copy_e("stats", "stats"), dgl.function.mean("stats", "stats")
    )
    g_rev = g.reverse(copy_ndata=True, copy_edata=True)
    g_rev.update_all(
        dgl.function.copy_e("stats", "stats"), dgl.function.mean("stats", "stats")
    )
    g.ndata["opp_stats"] = g_rev.ndata["stats"]
    g.edata["home"] = torch.from_numpy(games["HOME"].values.astype(np.float32))
    g.edata["plus_minus"] = torch.from_numpy(
        games["PLUS_MINUS"].values.astype(np.float32)
    )
    g.edata["date"] = torch.from_numpy(
        games[date_column].str.replace("-", "").astype(int).values
    )
    g.edata["season"] = torch.from_numpy(
        games[season_column].astype(str).str.slice(-4).astype(int).values
    )

    return g
