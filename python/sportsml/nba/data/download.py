import datetime
import pandas as pd
import time
import tqdm
from nba_api.stats.static import teams
from nba_api.stats.endpoints import leaguegamefinder


def download_games():
    games = []
    for team in tqdm.tqdm(teams.get_teams()):
        gamefinder = leaguegamefinder.LeagueGameFinder(team_id_nullable=team["id"])
        games.append(gamefinder.get_data_frames()[0])
        # try not to overload API service
        time.sleep(0.5)
    return pd.concat(games)
