import logging
from typing import List

import dgl
import numpy as np
import pandas as pd
import torch
import tqdm

from .features import STATS_COLUMNS
from .graph import graph_from_games
from .utils import process_games


class NBAGraphDataset(dgl.data.DGLDataset):
    def __init__(
        self,
        games: pd.DataFrame,
        stats_columns: List[str] = STATS_COLUMNS,
        label_columns: List[str] = None,
        date_column: str = "GAME_DATE",
        season_column: str = "SEASON_ID",
    ):
        self.stats_columns = stats_columns
        self.label_columns = label_columns or ["PLUS_MINUS"]
        self.date_column = date_column
        self.season_column = season_column

        self.games = games
        self.dates = self.games[self.date_column].unique().tolist()
        self.filter_valid_dates()

    def filter_valid_dates(self):
        logging.info("Filtering valid dates")
        valid_dates = []
        for idx, date in tqdm.tqdm(enumerate(self.dates), total=len(self.dates)):
            season = self.games[self.games[self.date_column] == date].iloc[0][
                self.season_column
            ]
            games = self.games[
                (self.games[self.date_column] < date)
                & (self.games[self.season_column] == season)
            ]
            if games["TEAM_ID"].unique().size == 30:
                valid_dates.append(date)
        self.dates = valid_dates

    def process(self, idx):
        date = self.dates[idx]
        season = self.games[self.games[self.date_column] == date].iloc[0][
            self.season_column
        ]

        games = self.games[
            (self.games[self.date_column] < date)
            & (self.games[self.season_column] == season)
        ]

        graph = graph_from_games(games)

        label_games = self.games[self.games[self.date_column] == date]

        label_graph = dgl.graph(
            (
                torch.from_numpy(label_games["src"].values.astype(np.int64)),
                torch.from_numpy(label_games["target"].values.astype(np.int64)),
            )
        )

        label_graph.edata["target"] = torch.from_numpy(
            label_games[self.label_columns].values.astype(np.float32)
        )

        label_graph.edata["home"] = torch.from_numpy(
            label_games[["HOME"]].values.astype(np.float32)
        )

        label_graph.edata["rest"] = torch.from_numpy(
            label_games[["REST"]].values.astype(np.float32)
        )

        return graph, label_graph

    def __len__(self):
        return len(self.dates)

    def __getitem__(self, idx):
        return self.process(idx)
