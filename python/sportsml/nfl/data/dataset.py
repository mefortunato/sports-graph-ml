from typing import List

import dgl
import numpy as np
import pandas as pd
import torch

from .features import STATS_COLUMNS, OPP_STATS_COLUMNS
from ...mongo import client


class NFLGamesDataset(object):
    def __init__(
        self,
        games: pd.DataFrame,
        stats_columns: List[str] = STATS_COLUMNS + OPP_STATS_COLUMNS,
        task_names: List[str] = ["result"],
    ):
        self.games = games
        self.weeks = (
            games.loc[games["week"] != 1, ["season", "week"]]
            .drop_duplicates()
            .values.tolist()
        )
        team_map = {
            team: _id for _id, team in enumerate(sorted(games["team"].unique()))
        }
        self.graph = dgl.graph(
            (games["opp_team"].map(team_map).values, games["team"].map(team_map).values)
        )
        self.graph.edata["season"] = torch.from_numpy(games["season"].values)
        self.graph.edata["week"] = torch.from_numpy(games["week"].values)
        self.graph.edata["e"] = torch.from_numpy(
            games[stats_columns].values.astype(np.float32)
        )
        self.graph.edata["target"] = torch.from_numpy(
            games[task_names].values.astype(np.float32)
        )

    @classmethod
    def from_mongo(cls):
        games = pd.DataFrame(client.nfl.games.find({}))
        games = games[games["completions"].notna()]
        games[STATS_COLUMNS] = games[STATS_COLUMNS].astype(np.float32)
        return cls(games)

    def __len__(self):
        return len(self.weeks)

    def __getitem__(self, idx):
        season, week = self.weeks[idx]
        train_graph = dgl.subgraph.edge_subgraph(
            self.graph,
            self.graph.filter_edges(
                lambda x: (x.data["season"] == season) & (x.data["week"] < week)
            ),
            relabel_nodes=False,
        )
        predict_graph = dgl.subgraph.edge_subgraph(
            self.graph,
            self.graph.filter_edges(
                lambda x: (x.data["season"] == season) & (x.data["week"] == week)
            ),
            relabel_nodes=False,
        )

        return train_graph, predict_graph
