import pandas as pd


def get_play_by_play():
    data = pd.read_parquet(
        r"https://github.com/nflverse/nflverse-data/releases/download/player_stats/player_stats.parquet"
    )
    return data


def get_game_totals():
    data = get_play_by_play()
    game_totals = data.groupby(["recent_team", "season", "week"]).sum().reset_index()
    return game_totals


def get_schedule():
    schedule = pd.pandas.read_csv(
        "https://github.com/nflverse/nfldata/raw/master/data/games.csv"
    )
    return schedule
