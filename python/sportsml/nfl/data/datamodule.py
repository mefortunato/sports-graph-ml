from typing import List

import dgl
import numpy as np
import pandas as pd
import pytorch_lightning as pl
from sklearn.preprocessing import StandardScaler

from .dataset import NFLGamesDataset
from .features import STATS_COLUMNS, OPP_STATS_COLUMNS
from ...mongo import client


class NFLGamesDataModule(pl.LightningDataModule):
    def __init__(
        self,
        batch_size: int = 64,
        pin_memory: bool = True,
        num_workers: int = 8,
        stats_columns: List[str] = STATS_COLUMNS + OPP_STATS_COLUMNS,
        task_names: List[str] = ["result"],
    ):
        super().__init__()
        self.batch_size = batch_size
        self.pin_memory = pin_memory
        self.num_workers = num_workers
        self.target_scaler = StandardScaler()

        self.task_names = task_names

        games = pd.DataFrame(client.nfl.games.find({}))
        games = games.dropna(subset=stats_columns)
        games[stats_columns] = games[stats_columns].astype(np.float32)
        seasons = sorted(games["season"].unique())
        train_seasons = seasons[:-3]
        val_seasons = seasons[-3:-2]
        test_seasons = seasons[-2:-1]
        self.target_scaler.fit(
            games[games["season"].isin(train_seasons)][self.task_names].values
        )
        games[self.task_names] = self.target_scaler.transform(
            games[self.task_names].values
        )
        self.train_ds = NFLGamesDataset(
            games[games["season"].isin(train_seasons)],
            stats_columns=stats_columns,
            task_names=task_names,
        )
        self.val_ds = NFLGamesDataset(
            games[games["season"].isin(val_seasons)],
            stats_columns=stats_columns,
            task_names=task_names,
        )
        self.test_ds = NFLGamesDataset(
            games[games["season"].isin(test_seasons)],
            stats_columns=stats_columns,
            task_names=task_names,
        )

    def setup(self, stage=""):
        pass

    def train_dataloader(self):
        return dgl.dataloading.GraphDataLoader(
            self.train_ds,
            batch_size=self.batch_size,
            pin_memory=self.pin_memory,
            num_workers=self.num_workers,
            shuffle=True,
        )

    def val_dataloader(self):
        return dgl.dataloading.GraphDataLoader(
            self.val_ds,
            batch_size=self.batch_size,
            pin_memory=self.pin_memory,
            num_workers=self.num_workers,
            shuffle=False,
        )

    def test_dataloader(self):
        return dgl.dataloading.GraphDataLoader(
            self.test_ds,
            batch_size=self.batch_size,
            pin_memory=self.pin_memory,
            num_workers=self.num_workers,
            shuffle=False,
        )

    def predict_dataloader(self):
        return dgl.dataloading.GraphDataLoader(
            self.test_ds,
            batch_size=self.batch_size,
            pin_memory=self.pin_memory,
            num_workers=self.num_workers,
            shuffle=False,
        )
