import hydra
import numpy as np
import pandas as pd
from omegaconf import OmegaConf, DictConfig


@hydra.main(config_path="configs", config_name="train", version_base=None)
def train(cfg: DictConfig):
    dm = hydra.utils.instantiate(cfg.data)
    model = hydra.utils.instantiate(cfg.model, target_scaler=dm.target_scaler)
    trainer = hydra.utils.instantiate(cfg.trainer)

    trainer.fit(model, dm)

    trainer.test(model, dm, ckpt_path="best")

    preds = pd.DataFrame()
    preds[dm.task_names] = trainer.predict(model, dm)[0]
    preds[dm.task_names] = model.target_scaler.inverse_transform(preds[dm.task_names])

    targets = pd.DataFrame()
    targets[dm.task_names] = np.vstack([pg.edata["target"] for tg, pg in dm.test_ds])
    targets[dm.task_names] = model.target_scaler.inverse_transform(
        targets[dm.task_names].values
    )

    preds.to_csv("preds.csv", index=False)
    targets.to_csv("targets.csv", index=False)
