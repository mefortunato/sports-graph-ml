import dgl
import torch

from .base import BaseModel


def concat_feat(edges):
    return {"h": torch.cat([edges.src["h"], edges.dst["h"]], dim=1)}


class AvgStatsModel(BaseModel):
    def __init__(self, in_feats, out_feats, metrics={}, target_scaler=None):
        super().__init__()
        self.lin = torch.nn.Linear(2 * in_feats, out_feats)
        self.batch_norm = torch.nn.BatchNorm1d(in_feats)
        self.target_scaler = target_scaler

        self.metrics = metrics
        for name, metric in self.metrics.items():
            setattr(self, name, metric)

        self.save_hyperparameters("in_feats", "target_scaler")

    def forward(self, train_graph, predict_graph):
        with train_graph.local_scope():
            with predict_graph.local_scope():
                train_graph.update_all(
                    dgl.function.copy_e("e", "m"), dgl.function.mean("m", "h")
                )
                predict_graph.ndata["h"] = train_graph.ndata["h"]
                predict_graph.ndata["h"] = self.batch_norm(predict_graph.ndata["h"])
                predict_graph.apply_edges(concat_feat)
                return self.lin(predict_graph.edata["h"])
