import dgl
import pytorch_lightning as pl
import torch
import torchmetrics
from dgllife.model import HadamardLinkPredictor, WLN
from sklearn.preprocessing import StandardScaler


class BaseModel(pl.LightningModule):
    def __init__(
        self,
        loss_fn=torch.nn.functional.mse_loss,
        learning_rate=1e-3,
        metrics={},
    ):
        super().__init__()
        self.loss_fn = loss_fn
        self.learning_rate = learning_rate
        self.target_scaler = None
        self.save_hyperparameters("loss_fn", "learning_rate")
        self.metrics = metrics
        for name, metric in self.metrics.items():
            setattr(self, name, metric)

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.learning_rate)
        return optimizer

    def step(self, batch):
        graph, predict_graph = batch
        preds = self(graph, predict_graph)
        targets = predict_graph.edata["target"]
        loss = self.loss_fn(preds, targets)
        return preds, targets, loss

    def training_step(self, batch, batch_idx):
        preds, targets, loss = self.step(batch)
        return loss

    def validation_step(self, batch, batch_idx):
        batch_size = batch[1].num_edges()
        preds, targets, loss = self.step(batch)
        self.log("val_loss", loss, batch_size=batch_size)

        if self.target_scaler is not None:
            preds = self.target_scaler.inverse_transform(preds.detach().cpu().numpy())
            preds = torch.from_numpy(preds).to(self.device)
            targets = self.target_scaler.inverse_transform(targets.cpu().numpy())
            targets = torch.from_numpy(targets).to(self.device)

        for name, metric in self.metrics.items():
            metric(preds, targets)
            self.log(
                f"val_{name}", metric, batch_size=batch_size, metric_attribute=name
            )

        return loss

    def test_step(self, batch, batch_idx):
        batch_size = batch[1].num_edges()
        preds, targets, loss = self.step(batch)
        self.log("test_loss", loss, batch_size=batch_size)

        if self.target_scaler is not None:
            preds = self.target_scaler.inverse_transform(preds.detach().cpu().numpy())
            preds = torch.from_numpy(preds).to(self.device)
            targets = self.target_scaler.inverse_transform(targets.cpu().numpy())
            targets = torch.from_numpy(targets).to(self.device)

        for name, metric in self.metrics.items():
            metric(preds, targets)
            self.log(
                f"test_{name}", metric, batch_size=batch_size, metric_attribute=name
            )

        return loss

    def predict_step(self, batch, batch_idx):
        graph, predict_graph = batch
        preds = self(graph, predict_graph)
        return preds
