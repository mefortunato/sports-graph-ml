from typing import List

import pymongo

from .client import client
from ..nba.data.features import STATS_COLUMNS as NBA_STATS
from ..nfl.data.features import STATS_COLUMNS as NFL_STATS


def averages(
    collection: pymongo.collection.Collection,
    stats: List[str],
    team_key: str,
    season: int,
    season_key: str,
    date: str,
    date_key: str,
):
    stats_agg = {col: {"$avg": f"${col}"} for col in stats}
    group_agg = {"$group": {"_id": f"${team_key}", "count": {"$count": {}}}}
    group_agg["$group"].update(stats_agg)
    return list(
        collection.aggregate(
            [
                {"$match": {season_key: season, date_key: {"$lt": date}}},
                group_agg,
            ]
        )
    )


def nba_averages(season: int, date: str, stats: List[str] = None):
    if stats is None:
        stats = NBA_STATS + [f"{stat}_OPP" for stat in NBA_STATS]
    return averages(
        collection=client.nba.games,
        stats=stats,
        team_key="TEAM_ABBREVIATION",
        season=season,
        season_key="SEASON_ID",
        date=date,
        date_key="GAME_DATE",
    )


def nfl_averages(season: int, date: str, stats: List[str] = None):
    if stats is None:
        stats = NFL_STATS + [f"opp_{stat}" for stat in NFL_STATS]
    return averages(
        collection=client.nfl.games,
        stats=stats,
        team_key="team",
        season=season,
        season_key="season",
        date=date,
        date_key="week",
    )
