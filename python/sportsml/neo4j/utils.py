from py2neo import Node

from .graph import graph


def create_node_id_constraint():
    graph.run(
        "CREATE CONSTRAINT team_id IF NOT EXISTS for (t:Team) REQUIRE t._id IS UNIQUE"
    )


def create_team(_id, abbreviation, name):
    tx = graph.begin()
    team = Node("Team", _id=_id, abbreviation=abbreviation, name=name)
    tx.create(team)
    tx.commit()
