import os

import pandas as pd
import typer
from pymongo import ReplaceOne

from sportsml.mongo import client
from sportsml.nba.data.download import download_games
from sportsml.nba.data.utils import process_games

from sportsml.nfl.data.download import get_game_totals, get_schedule
from sportsml.nfl.data.utils import merge_games_schedule

app = typer.Typer()


@app.command()
def nba(path: str = typer.Option(""), from_api: bool = typer.Option(True)):
    if not from_api and not path:
        raise ValueError('one of "--path" or "--from-api" must be provided')
    if from_api:
        games = download_games()
    else:
        games = pd.read_csv(path)
    games = process_games(games)
    games = games.drop(columns="Unnamed: 0")
    games["_id"] = games[["GAME_ID", "TEAM_ID"]].agg(
        lambda x: ".".join(map(str, x)), axis=1
    )
    updates = [
        ReplaceOne({"_id": game["_id"]}, game, upsert=True)
        for game in games.to_dict(orient="records")
    ]
    result = client.nba.games.bulk_write(updates)


@app.command()
def nfl():
    games = get_game_totals()
    schedule = get_schedule()
    games = merge_games_schedule(games, schedule)
    updates = [
        ReplaceOne({"_id": game["_id"]}, game, upsert=True)
        for game in games.to_dict(orient="records")
    ]
    result = client.nfl.games.bulk_write(updates)


if __name__ == "__main__":
    app()
