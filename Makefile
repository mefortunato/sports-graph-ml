REGISTRY ?= registry.gitlab.com/mefortunato/sports-graph-ml
VERSION ?= $(shell cd python && python setup.py --version)

default: build

freeze:
	@docker build \
		-t $(REGISTRY):freeze \
		-f docker/Dockerfile.freeze \
		.
	@docker run -it --rm \
		$(REGISTRY):freeze \
		python -m pip freeze > python/requirements.tmp
	@grep "^-" python/requirements.cpu.dev | cat - python/requirements.tmp > python/requirements.txt
	@rm python/requirements.tmp

build:
	@docker build \
		-t $(REGISTRY):$(VERSION) \
		-f docker/Dockerfile \
		.

push:
	@docker push $(REGISTRY):$(VERSION)

debug:
	@docker run -it --rm \
		-v $(shell pwd)/python/sportsml:/usr/local/lib/python3.9/site-packages/sportsml \
		$(VOLUMES) \
		--env-file .env \
		-w /project \
		--entrypoint bash \
		$(REGISTRY):$(VERSION)